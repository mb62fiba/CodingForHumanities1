# First Assignment

## Class Expectations


I assume this just is supposed to be some kind of lorem ipsum placeholder text.
However, in case it is not here are a few words on my expectations of this
class: After finishing my Master's thesis in political science last summer,
I felt that the approach I pursued within this thesis, which can be characterized as
computer-driven text analysis which was used to determine political preferences of political
actors on the basis of speeches they delivered and press statements they
released, had some potential if applied the right way. Packages for r like
*wordfish* enabled me to do so on my own. However as an autodidact (and way more
an political theorist than a computer scientist) both the preparation of the
text corpora and the analysis had some flaws which made things like text mining,
modelling as well as the interpretation and modification of the resulting tables
and graphs more cumbersome than probably necessary. Coming from this starting
point, I hope this course will make it possible for me to use the things I
already know in a more structured, informed and faster way. Of
particular interest for me are data visualization principles. After producing
long essays and theses for a while now, I certainly appreciate a well made graphical
representation of complex data.


### Autodidactic Basis

Websites I use as a self-taught r-user interested in text analysis:

* quantitative text mining on [textasdata.com](http://www.textasdata.com/)

* Some fancy applications of r, both descriptive statistics and more advanced
    stuff on [r-bloggers](https://www.r-bloggers.com/)

## Top Expectations

* No more hand-cleaning of 2000 parliamentary speeches

* Being able to have a look under the surface of r-packages. How does topical clustering work from a computer-scientist's view and how do I avoid weird outcomes and bad graphical representation?

* Better Visualization! Maps in r! What does aes do?
