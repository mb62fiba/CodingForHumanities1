# Coding for Digital Humanities
This is the markdown file that I created for the first assignment. 

### My Expectations: 
* learn the coding-for-DH basics
* learn how to apply the knowledge and practise as much as I can
* submit the first project successfully 

In the end, it is going to be like [this] https://cdn.meme.am/instances/64369464.jpg 